Spalchemy

Made for the first Alakajam!
Theme: Alchemy

Controls:
Dash forward: W or Up
Turning: A/D or Left/Right

You need Godot 2.1.4 to run or edit the game.

Download Godot at https://godotengine.org/download
