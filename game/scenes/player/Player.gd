extends KinematicBody2D

signal gold_count_changed(gold_count)

var velocity = null
var MAX_SPEED = 200
var ROTATION_SPEED = 2*PI
var gold_count = 0
var dashing = false

onready var dash_timer = get_node("DashTimer")
onready var animation = get_node("AnimationPlayer")

func _ready():
	velocity = Vector2(0, 0)
	gold_count = 0
	set_fixed_process(true)
	set_process_input(true)

func _fixed_process(delta):
	var rotation = 0
	if Input.is_action_pressed("turn_left"):
		rotation += ROTATION_SPEED * delta
	if Input.is_action_pressed("turn_right"):
		rotation -= ROTATION_SPEED * delta
	set_rot(get_rot() + rotation)
	
	var remainder = move(velocity * delta)
	if is_colliding():
		var normal = get_collision_normal()
		velocity = 0.6 * normal.slide(velocity)
		move(normal.slide(remainder))
	
	velocity = velocity.clamped(MAX_SPEED)

func _input(event):
	if not dashing and event.is_action_pressed("dash_forward"):
		velocity = 0.5 * velocity - 0.5 * MAX_SPEED * get_transform().y
		dashing = true
		dash_timer.start()
		animation.play("dash")


func add_gold():
	gold_count += 1
	emit_signal("gold_count_changed", gold_count)
	get_node("SamplePlayer").play("Pickup")


func _on_DashTimer_timeout():
	dashing = false
