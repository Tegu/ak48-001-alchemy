extends Area2D

signal goal_entered(player)

func _ready():
	pass


func _on_Goal_body_enter( body ):
	if body.get_name() == "Player":
		emit_signal("goal_entered", body)
