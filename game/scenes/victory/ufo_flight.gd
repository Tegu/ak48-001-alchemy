extends Sprite

onready var t = 0

func _ready():
	set_process(true)

func _process(delta):
	set_offset(Vector2(
		20*cos(2*PI*0.11*t),
		40*sin(2*PI*0.2*t)))
	set_rot(0.04*PI*sin(2*PI*0.01*t))
	t += delta