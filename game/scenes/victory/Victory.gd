extends Control

func _ready():
	var fader = get_node("CanvasLayer/Screenfader/AnimationPlayer")
	fader.play("fade_from_black")
	fader.connect("finished", fader, "queue_free")
	set_process_input(true)
	set_process(true)

func _input(event):
	if event.is_action_released("quit"):
		get_tree().quit()

func _process(delta):
	var camera = get_node("Camera2D")
	camera.set_pos(camera.get_pos() + 500 * Vector2(delta, 0))


func _on_RestartButton_button_up():
	get_tree().change_scene("res://scenes/main/Main.tscn")
