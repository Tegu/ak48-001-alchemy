	extends Node2D

onready var hud = get_node("CanvasLayer/HUD")
var needed_gold_count = 0

func _ready():
	initialize_player()
	initialize_gold_count()
	get_node("CanvasLayer/Screenfader/AnimationPlayer").play("fade_from_black")
	set_process_input(true)

func _input(event):
	if event.is_action_released("quit"):
		get_tree().quit()

func initialize_player():
	var player_spawn = get_node("Level/PlayerSpawn")
	var player = get_node("Player")
	player.set_global_transform(player_spawn.get_global_transform())

func initialize_gold_count():
	needed_gold_count = get_node("Level/Golds").get_child_count()
	hud.set_gold_status(0, needed_gold_count)

func goal_entered(player):
	if player.gold_count == needed_gold_count:
		_on_victory()

func _on_victory():
	var animation = get_node("CanvasLayer/Screenfader/AnimationPlayer")
	animation.play("fade_to_black")
	animation.connect("finished", self, "switch_to_victory")

func switch_to_victory():
	get_tree().change_scene("res://scenes/victory/Victory.tscn")


func _on_Player_gold_count_changed( gold_count ):
	hud.set_gold_status(gold_count, needed_gold_count)
