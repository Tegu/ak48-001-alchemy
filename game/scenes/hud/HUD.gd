extends Control

func _ready():
	pass

func set_gold_status(current, maximum):
	get_node("GoldStatus").set_text("Golds: " + str(current) + "/" + str(maximum))
